"""
CARPI GPS REPORTER DAEMON
(C) 2021, Raphael "rGunti" Guntersweilerr
Licensed under MIT
"""
from configparser import ConfigParser
from typing import Dict, Callable

from gpsreporter.targets.base import ReportingTarget


def _make_null_target(_: ConfigParser) -> ReportingTarget:
    from gpsreporter.targets.null import NullTarget
    return NullTarget()


def _make_http_get_target(config: ConfigParser) -> ReportingTarget:
    from gpsreporter.targets.http import HttpGetTarget
    return HttpGetTarget(config)


def _make_http_post_target(config: ConfigParser) -> ReportingTarget:
    from gpsreporter.targets.http import HttpPostTarget
    return HttpPostTarget(config)


def _make_log_target(_: ConfigParser) -> ReportingTarget:
    from gpsreporter.targets.log import LogTarget
    return LogTarget()


KNOWN_TARGETS: Dict[str, Callable[[ConfigParser], ReportingTarget]] = {
    'null': _make_null_target,
    'http_get': _make_http_get_target,
    'http_post': _make_http_post_target,
    'log': _make_log_target
}


def is_target_known(target: str) -> bool:
    return target in KNOWN_TARGETS


def get_target(target: str, config: ConfigParser) -> ReportingTarget:
    return KNOWN_TARGETS[target](config)
