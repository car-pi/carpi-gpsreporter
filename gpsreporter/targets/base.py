"""
CARPI GPS REPORTER DAEMON
(C) 2021, Raphael "rGunti" Guntersweilerr
Licensed under MIT
"""
from datetime import datetime
from typing import Optional

from carpicommons.errors import CarPiExitException
from carpicommons.log import logger


class GpsData(object):
    def __init__(self,
                 latitude: float,
                 longitude: float,
                 altitude: float,
                 accuracy: float,
                 speed: float,
                 heading: float,
                 timestamp: str = None,
                 battery_soc: Optional[float] = None):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.accuracy = accuracy
        self.speed = speed
        self.heading = heading
        self.timestamp = (timestamp if timestamp else datetime.now().isoformat())
        self.battery_soc = battery_soc or -1


class ReportingTargetConfigError(CarPiExitException):
    EXIT_CODE = 0xD601

    def __init__(self):
        super().__init__(ReportingTargetConfigError.EXIT_CODE)


class ReportingTarget(object):
    def __init__(self, target_name: str):
        self._name = target_name
        self._logger = logger('Target {}'.format(target_name))

    def report(self, data: GpsData):
        raise NotImplementedError()
