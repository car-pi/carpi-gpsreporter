"""
CARPI GPS REPORTER DAEMON
(C) 2021, Raphael "rGunti" Guntersweilerr
Licensed under MIT
"""
import time
from configparser import ConfigParser
from datetime import tzinfo

import pytz

import isodate
from requests import Response

from gpsreporter.targets.base import ReportingTarget, GpsData, ReportingTargetConfigError
import requests


def _construct_params_from_data(data: GpsData, tz: tzinfo, user_agent: str) -> dict:
    return {
        'lat': data.latitude,
        'lon': data.longitude,
        'alt': data.altitude,
        'acc': data.accuracy,
        'bat': data.battery_soc or -1,
        'sat': 0,
        'speed': data.speed,
        'bearing': data.heading,
        'timestamp': time.mktime(isodate.parse_datetime(data.timestamp).astimezone(tz).timetuple()),
        'useragent': user_agent
    }


class HttpTarget(ReportingTarget):
    def __init__(self, config: ConfigParser, name: str):
        super().__init__(name)
        config_sec = 'Target_{}'.format(name)
        self._base_url = config.get(config_sec, 'url')
        self._timezone = pytz.timezone(config.get(config_sec, 'tz') or 'UTC')
        self._user_agent = config.get(config_sec, 'useragent') or 'CarPi-GPSReporter'
        if not self._base_url:
            self._logger.error('No URL has been provided for target %s', self._name)
            raise ReportingTargetConfigError()

    def report(self, data: GpsData):
        params = _construct_params_from_data(data, self._timezone, self._user_agent)
        try:
            self._logger.debug('Sending data via HTTP GET to %s', self._base_url)
            resp = self._submit_request(params)
            self._logger.info('Sent, received Status Code %s, response was: %s',
                              resp.status_code,
                              resp.text)
        except BaseException as e:
            self._logger.warning('Request failed due to an exception, skipped over it. Exception was %s', e)

    def _submit_request(self, data: dict) -> Response:
        raise NotImplementedError()


class HttpGetTarget(HttpTarget):
    def __init__(self, config: ConfigParser):
        super().__init__(config, 'http_get')

    def _submit_request(self, data: dict) -> Response:
        return requests.get(self._base_url,
                            params=data)


class HttpPostTarget(HttpTarget):
    def __init__(self, config: ConfigParser):
        super().__init__(config, 'http_post')

    def _submit_request(self, data: dict) -> Response:
        return requests.post(self._base_url,
                             params=data)
