"""
CARPI GPS REPORTER DAEMON
(C) 2021, Raphael "rGunti" Guntersweilerr
Licensed under MIT
"""
from gpsreporter.targets.base import ReportingTarget, GpsData


class NullTarget(ReportingTarget):
    def __init__(self):
        super().__init__('null')

    def report(self, _: GpsData):
        pass
