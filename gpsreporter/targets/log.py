"""
CARPI GPS REPORTER DAEMON
(C) 2021, Raphael "rGunti" Guntersweilerr
Licensed under MIT
"""
import json

from gpsreporter.targets.base import ReportingTarget, GpsData


class LogTarget(ReportingTarget):
    def __init__(self):
        super().__init__('log')

    def report(self, data: GpsData):
        j = {}
        j.update(vars(data))
        self._logger.info('Reporting new values: %s',
                          json.dumps(j,
                                     skipkeys=True,
                                     indent=4))
